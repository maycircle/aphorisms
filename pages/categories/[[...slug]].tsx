import Head from "next/head";
import Layout from "../../components/layout";
import AphorismList from "../../components/aphorismlist";
import AphorismEntry, { Aphorism } from "../../components/aphorismentry";
import { useRouter } from "next/router";
import { QuotableApi, QuoteList, Tag } from "../../quotable-api";
import { useEffect, useRef, useState } from "react";
import aphorism from "../../components/aphorismentry.module.css";
import { useLocalStorage } from "../_app";

const CATEGORY_ALL: Tag = { id: "all", title: "All" };
const isCategoryIdAll = (id: string) => id == CATEGORY_ALL.id;

export default function Categories() {
    const router = useRouter();
    const api = useRef(new QuotableApi());
    const [favorites, setFavorites] = useLocalStorage<Aphorism[]>("favorites", []);

    const [page, setPage] = useState(1);
    const [category, setCategory] = useState("all");
    const [quotes, setQuotes] = useState<QuoteList | null>(null);
    const [switchUpdate, setSwitchUpdate] = useState(false);

    useEffect(() => {
        if (router.query["page"] && !Array.isArray(router.query["page"])) { 
            setPage(Math.max(1, parseInt(router.query["page"])));
        }
        if (Array.isArray(router.query["slug"])) {
            setCategory(router.query["slug"][0]);
        }
        setSwitchUpdate(!switchUpdate);
    }, [router.isReady]);

    useEffect(() => {
        if (!router.isReady) {
            return;
        }

        const tag = isCategoryIdAll(category) ? undefined : category;
        api.current.list({ page, tags: tag }).then(data => {
            setQuotes(data);
        });
    }, [page, category, switchUpdate]);

    const handleChangePage = (page: number) => {
        let path = router.asPath;
        let queryStart = path.indexOf("?");
        if (queryStart == -1) {
            queryStart = path.length;
        }

        const route = path.substring(0, queryStart);
        router.push(route + "?page=" + page.toString());
        setPage(page);
    };

    const handleChangeCategory = (id: string) => {
        router.push(id, undefined, { shallow: true });
        setPage(1);
        setCategory(id);
    }

    const handleChangeFavoriteState = (isFavorite: boolean, props: Aphorism) => {
        let tmp = favorites.concat();
        if (!isFavorite) {
            tmp.push(props);
        } else {
            tmp = tmp.filter(x => x.id != props.id);
        }
        setFavorites(tmp);
    };

    return (
        <Layout>
            <Head>
                <title>Aphorisms&trade; Categories</title>
            </Head>
            <AphorismList
                page={page}
                pageCount={quotes?.totalPages ?? 0}
                onChangePage={handleChangePage}
                count={quotes?.count}
                totalCount={quotes?.totalCount}
                lastQuoteIndex={quotes?.lastItemIndex}
                categories={getCategoriesCached(api.current)}
                selectedCategoryId={category}
                onChangeCategory={handleChangeCategory}
            >
                <ul>
                    {quotes?.results?.map((quote, index) => (
                        <li className={aphorism.aphorism} key={quote._id}>
                            <div
                                className={`px-4 sm:px-10 py-5 ${index % 2 == 0 ?
                                        "bg-gray-50" :
                                        "bg-white"
                                    }`}
                            >
                                <AphorismEntry
                                    id={quote._id}
                                    author={quote.author}
                                    text={quote.content}
                                    isFavorite={!favorites.find(x => x.id == quote._id)}
                                    onChangeFavoriteState={handleChangeFavoriteState}
                                />
                            </div>
                        </li>
                    )) ?? <></>}
                </ul>
            </AphorismList>
        </Layout>
    );
}

function getCategoriesCached(api: QuotableApi): Tag[] {
    return [CATEGORY_ALL].concat(api.tagsCached());
}
