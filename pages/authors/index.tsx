import Head from "next/head";
import Layout from "../../components/layout";

export default function Categories() {
    return (
        <Layout>
            <Head>
                <title>Aphorisms&trade; Categories</title>
            </Head>
        </Layout>
    );
}
