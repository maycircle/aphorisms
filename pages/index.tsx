import Head from "next/head"
import Layout from "../components/layout"
import AphorismEntry, { Aphorism } from "../components/aphorismentry";
import LightAphorismList from "../components/lightaphorismlist";
import { QuotableApi, QuoteList, Tag } from "../quotable-api";
import { useEffect, useRef, useState } from "react";
import aphorism from "../components/aphorismentry.module.css";
import { useLocalStorage } from "./_app";

const CATEGORY_ALL: Tag = { id: "all", title: "All" };
const isCategoryIdAll = (id: string) => id == CATEGORY_ALL.id;

export default function Home() {
    const api = useRef(new QuotableApi());
    const [favorites, setFavorites] = useLocalStorage<Aphorism[]>("favorites", []);

    const [category, setCategory] = useState("all");
    const [quotes, setQuotes] = useState<QuoteList | null>(null);

    useEffect(() => {
        const tag = isCategoryIdAll(category) ? undefined : category;
        api.current.list({ page: 1, limit: 2, tags: tag }).then(data => {
            setQuotes(data);
        });
    }, [category]);

    const handleChangeCategory = (id: string) => {
        setCategory(id);
    };

    const handleChangeFavoriteState = (isFavorite: boolean, props: Aphorism) => {
        let tmp = favorites.concat();
        if (!isFavorite) {
            tmp.push(props);
        } else {
            tmp = tmp.filter(x => x.id != props.id);
        }
        setFavorites(tmp);
    };

    return (
        <Layout>
            <Head>
                <title>Aphorisms&trade;</title>
            </Head>
            <LightAphorismList
                categories={getCategoriesCached(api.current)}
                onChangeCategory={handleChangeCategory}
            >
                <ul>
                    {quotes?.results?.map((quote, index) => (
                        <li className={aphorism.aphorism} key={quote._id}>
                            <div
                                className={`px-4 sm:px-10 py-5 ${index % 2 == 0 ?
                                    "bg-gray-50" :
                                    "bg-white"
                                    }`}
                            >
                                <AphorismEntry
                                    id={quote._id}
                                    author={quote.author}
                                    text={quote.content}
                                    isFavorite={!favorites.find(x => x.id == quote._id)}
                                    onChangeFavoriteState={handleChangeFavoriteState}
                                />
                            </div>
                        </li>
                    )) ?? <></>}
                </ul>
            </LightAphorismList>
            <article>
                <div
                    className="mb-4 bg-black"
                    style={
                        {
                            fontFamily: "Oswald"
                        }
                    }
                >
                    <h1 className="p-4 text-center uppercase text-6xl text-white">
                        This could be your ad
                    </h1>
                </div>
                <p>
                    An aphorism (from Greek ἀφορισμός: aphorismos, denoting
                    'delimitation', 'distinction', and 'definition') is a
                    concise, terse, laconic, or memorable expression of a
                    general truth or principle. Aphorisms are often handed
                    down by tradition from generation to generation.
                </p>
                <p>
                    The concept is generally distinct from those of an adage,
                    brocard, chiasmus, epigram, maxim (legal or philosophical),
                    principle, proverb, and saying; although some of these
                    concepts may be construed as types of aphorism.
                </p>
            </article>
        </Layout>
    );
}

function getCategoriesCached(api: QuotableApi): Tag[] {
    return [CATEGORY_ALL].concat(api.tagsCached().slice(0, 2));
}
