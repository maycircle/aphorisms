import "reflect-metadata";
import "../styles/global.css"
import { AppProps } from "next/app"
import { useEffect, useState } from "react";

export default function App({ Component, pageProps }: AppProps) {
    return <Component {...pageProps} />
}

export function useLocalStorage<T>(key: string, fallbackValue: T) {
    const [value, setValue] = useState(fallbackValue);
    useEffect(() => {
        const stored = localStorage.getItem(key);
        setValue(stored ? JSON.parse(stored) : fallbackValue);
    }, []);

    useEffect(() => {
        localStorage.setItem(key, JSON.stringify(value));
    }, [value]);

    return [value, setValue] as const;
}
