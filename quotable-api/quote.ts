import { Expose } from "class-transformer";

export class Quote {
    @Expose()
    _id: string | undefined;

    @Expose()
    content: string | undefined;

    @Expose()
    author: string | undefined;

    @Expose()
    authorSlug: string | undefined;

    @Expose()
    length: number | undefined;

    @Expose()
    tags: string[] | undefined;
}
