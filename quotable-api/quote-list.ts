import { Expose, Type } from "class-transformer";
import { Quote } from "./quote";

export class QuoteList {
    public static readonly API_ENDPOINT = "/quotes";

    @Expose()
    public count: number | undefined;

    @Expose()
    totalCount: number | undefined;

    @Expose()
    page: number | undefined;

    @Expose()
    totalPages: number | undefined;

    @Expose()
    lastItemIndex: number | undefined | null;

    @Expose()
    @Type(() => Quote)
    results: Quote[] | undefined;
}
