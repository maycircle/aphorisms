import { plainToClass } from "class-transformer";
import { QuoteList } from "./quote-list";
import { Tag } from "./tag";

export enum ListSortBy {
    DateAdded = "dateAdded",
    DateModified = "dateModified",
    Author = "author",
    Content = "content",
}

export enum ListOrder {
    Ascending = "asc",
    Descending = "desc"
}

export interface IQuotableListOptions {
    maxLength?: number;
    minLength?: number;
    tags?: string;
    author?: string;
    sortBy?: ListSortBy; // default: ListSortBy.DateAdded
    order?: ListOrder;   // default: depends on sortBy
    limit?: number;      // min: 1, max: 150, default: 20
    page?: number;       // min: 1, default: 1
}

export class QuotableApi {
    public static readonly API_ORIGIN = "https://api.quotable.io";

    public async list(options?: IQuotableListOptions): Promise<QuoteList> {
        const url = new URL(
            QuoteList.API_ENDPOINT,
            QuotableApi.API_ORIGIN
        );

        if (options != undefined) {
            for (let option of Object.keys(options) as [keyof IQuotableListOptions]) {
                const field = options[option];
                if (!field) {
                    continue;
                }

                url.searchParams.append(option, field.toString());
            }
        }

        return fetch(url).then(res => {
            if (!res.ok) {
                throw new Error(res.statusText);
            }
            return res.json();
        }).then(data => {
            let cls = plainToClass(QuoteList, data,
                { excludeExtraneousValues: true });
            return cls;
        });
    }

    public tagsCached(): Tag[] {
        return [
            { id: "wisdom",         title: "Wisdom"         },
            { id: "happiness",      title: "Happiness"      },
            { id: "honor",          title: "Honor"          },
            { id: "philosophy",     title: "Philosophy"     },
            { id: "love",           title: "Love"           },
            { id: "virtue",         title: "Virtue"         },
            { id: "self-help",      title: "Self-help"      },
            { id: "truth",          title: "Truth"          },
            { id: "character",      title: "Character"      },
            { id: "courage",        title: "Courage"        },
            { id: "motivational",   title: "Motivational"   },
            { id: "life",           title: "Life"           },
            { id: "self",           title: "Self"           },
            { id: "sports",         title: "Sports"         },
            { id: "competition",    title: "Competition"    },
            { id: "business",       title: "Business"       },
            { id: "famous-quotes",  title: "Famous quotes"  },
            { id: "humorous",       title: "Humorous"       },
            { id: "success",        title: "Success"        },
            { id: "pain",           title: "Pain"           },
            { id: "change",         title: "Change"         },
            { id: "humor",          title: "Humor"          },
            { id: "politics",       title: "Politics"       },
            { id: "war",            title: "War"            },
            { id: "history",        title: "History"        },
            { id: "spirituality",   title: "Spirituality"   },
            { id: "faith",          title: "Faith"          },
            { id: "inspirational",  title: "Inspirational"  },
            { id: "technology",     title: "Technology"     },
            { id: "conservative",   title: "Conservative"   },
            { id: "social-justice", title: "Social justice" },
            { id: "freedom",        title: "Freedom"        },
            { id: "friendship",     title: "Friendship"     },
            { id: "science",        title: "Science"        },
            { id: "film",           title: "Film"           },
            { id: "future",         title: "Future"         },
            { id: "leadership",     title: "Leadership"     },
            { id: "religion",       title: "Religion"       },
            { id: "power-quotes",   title: "Power quotes"   },
            { id: "education",      title: "Education"      },
            { id: "nature",         title: "Nature"         },
            { id: "literature",     title: "Literature"     },
        ];
    }
}
