import styles from "./aphorismentry.module.css"
import { useEffect, useState } from "react";

export interface Aphorism {
    id?: String
    text?: String
    author?: String
}

interface Props {
    id?: String
    text?: String
    author?: String
    isFavorite?: boolean
    onChangeFavoriteState?: (isFavorite: boolean, props: Aphorism) => void
}

export default function AphorismEntry({ ...props }: Props) {
    const [isFavorite, setFavorite] = useState(props.isFavorite ?? true);
    function handleAddToFavorites() {
        if (!props.onChangeFavoriteState) {
            return;
        }

        setFavorite(!isFavorite);
        props.onChangeFavoriteState(!isFavorite,
            { id: props.id, text: props.text, author: props.author });
    }

    return (
        <>
            <p className="font-serif italic text-gray-800">
                <span className={`${styles.quoteFace} text-2xl`}>
                    &bdquo;{props.text ? props.text[0] : ""}&thinsp;
                </span>
                {props.text ? props.text.substring(1) : ""}
                <span className={`${styles.quoteFace} text-xl`}>
                    &quot;&nbsp;
                </span>
            </p>
            <div className="flex pt-2 items-center">
                <div className="mr-auto">
                    <p className="whitespace-nowrap text-slate-700">
                        &mdash; {props.author}
                    </p>
                </div>
                <div className={styles.actionBar}>
                    <button
                        className="p-1 pr-2 rounded-xl bg-gray-100"
                        onClick={handleAddToFavorites}
                    >
                        <div className="flex items-center">
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 96 960 960"
                            >
                                {isFavorite ? (
                                    <path
                                        d="m480 935-41-37q-105.768-97.121-174.884-167.561Q195 660 154 604.5T96.5 504Q80 459 80 413q0-90.155 60.5-150.577Q201 202 290 202q57 0 105.5 27t84.5 78q42-54 89-79.5T670 202q89 0 149.5 60.423Q880 322.845 880 413q0 46-16.5 91T806 604.5Q765 660 695.884 730.439 626.768 800.879 521 898l-41 37Zm0-79q101.236-92.995 166.618-159.498Q712 630 750.5 580t54-89.135q15.5-39.136 15.5-77.72Q820 347 778 304.5T670.225 262q-51.524 0-95.375 31.5Q531 325 504 382h-49q-26-56-69.85-88-43.851-32-95.375-32Q224 262 182 304.5t-42 108.816Q140 452 155.5 491.5t54 90Q248 632 314 698t166 158Zm0-297Z" />
                                ) : (
                                    <path d="m480 935-41-37q-106-97-175-167.5t-110-126Q113 549 96.5 504T80 413q0-90 60.5-150.5T290 202q57 0 105.5 27t84.5 78q42-54 89-79.5T670 202q89 0 149.5 60.5T880 413q0 46-16.5 91T806 604.5q-41 55.5-110 126T521 898l-41 37Z" />
                                )}
                            </svg>
                            <p className="ml-2 text-right">
                                {isFavorite ?
                                    "Favorite" :
                                    "Unfavorite"
                                }
                            </p>
                        </div>
                    </button>
                </div>
            </div>
        </>
    );
}
