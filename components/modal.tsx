import { ReactNode } from "react";
import styles from "./modal.module.css";

interface Props {
    visible: boolean
    children?: ReactNode
}

export default function Modal({ children, ...props }: Props) {
    return (
        <>
            <div className={
                props.visible ?
                styles['blur-visible'] :
                styles['blur-hidden']
            }/>
            <nav className={
                props.visible ?
                styles['portal-visible'] :
                styles['portal-hidden']
            }>
                <div
                    className="bg-gray-900"
                    onTouchStart={e => e.stopPropagation()}
                >
                    {children}
                </div>
            </nav>
        </>
    );
}
