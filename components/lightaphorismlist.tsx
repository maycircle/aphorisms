import Link from "next/link";
import styles from "./aphorismentry.module.css"
import { Children, MouseEventHandler, ReactNode, useState } from "react";
import { Tag } from "../quotable-api";

interface Props {
    children?: ReactNode
    categories?: Tag[]
    onChangeCategory?: (id: string) => void
}

export default function LightAphorismList({ children, ...props }: Props) {
    const [selectedCategoryId, setSelectedCategoryId] = useState("all");
    const handleChangeCategory: MouseEventHandler = e => {
        if (props.onChangeCategory === undefined) {
            return;
        }

        const id: string | null = e.currentTarget.getAttribute("data-tid");
        if (id != null) {
            setSelectedCategoryId(id);
            props.onChangeCategory(id);
        }
    };

    return (
        <div className="mb-4 sm:grid sm:grid-cols-4 sm:gap-3">
            <div className="overflow-hidden sm:col-span-3">
                <div className="bg-white rounded-lg pb-1 sm:border">
                    <div className="px-4 py-5 sm:px-6">

                        <h2 className="font-semibold text-2xl text-pink-950">
                            &#127892; Most Loved
                        </h2>
                    </div>
                    <div className="border-t border-gray-200">
                        {children}
                    </div>
                </div>
            </div>
            <div>
                <div className="overflow-hidden bg-white rounded-lg border">
                    <ul>
                        {props.categories?.map(category => (
                            <li key={category.id}>
                                <div
                                    className={
                                        `p-4 ${category.id == selectedCategoryId ?
                                            `text-sky-500 fill-sky-500` :
                                            `text-gray-600 fill-gray-600 hover:text-black`} cursor-pointer`
                                    }
                                    onClick={handleChangeCategory}
                                    data-tid={category.id}
                                >
                                    <p className="text-lg">{category.title}</p>
                                </div>
                            </li>
                        ))}
                        <li>
                            <Link
                                href="/categories"
                                className="flex items-center p-4 text-gray-600 fill-gray-600 hover:text-black hover:fill-black cursor-pointer"
                            >
                                <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 96 960 960"
                                >
                                    <path
                                        d="M207.858 624Q188 624 174 609.858q-14-14.141-14-34Q160 556 174.142 542q14.141-14 34-14Q228 528 242 542.142q14 14.141 14 34Q256 596 241.858 610q-14.141 14-34 14Zm272 0Q460 624 446 609.858q-14-14.141-14-34Q432 556 446.142 542q14.141-14 34-14Q500 528 514 542.142q14 14.141 14 34Q528 596 513.858 610q-14.141 14-34 14Zm272 0Q732 624 718 609.858q-14-14.141-14-34Q704 556 718.142 542q14.141-14 34-14Q772 528 786 542.142q14 14.141 14 34Q800 596 785.858 610q-14.141 14-34 14Z"
                                    />
                                </svg>
                                <p className="ml-2 text-lg">
                                    More
                                </p>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}
