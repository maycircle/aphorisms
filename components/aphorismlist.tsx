import { MouseEventHandler, ReactNode, useEffect, useState } from "react";
import Pagination, { PaginationProps } from "./pagination";
import { Tag } from "../quotable-api";

interface Props extends PaginationProps {
    children?: ReactNode
    count?: number
    totalCount?: number
    lastQuoteIndex?: number | null
    categories?: Tag[]
    selectedCategoryId?: string
    onChangeCategory?: (id: string) => void
}

export default function AphorismList({ children, ...props }: Props) {
    const [selectedCategoryId, setSelectedCategoryId] = useState(props.selectedCategoryId ?? "all");
    const [isCategoriesMinimized, setCategoriesMinimized] = useState(true);

    useEffect(() => {
        setSelectedCategoryId(props.selectedCategoryId ?? "all");
    }, [props.selectedCategoryId]);

    const handleChangeCategory: MouseEventHandler = e => {
        if (props.onChangeCategory === undefined) {
            return;
        }

        const id: string | null = e.currentTarget.getAttribute("data-tid");
        if (id != null) {
            setSelectedCategoryId(id);
            props.onChangeCategory(id);
        }
    };

    const handleShowMore = () => {
        setCategoriesMinimized(!isCategoriesMinimized);
    };

    let label: string;
    if (!props.count || !props.totalCount) {
        label = `Showing 0`;
    } else if (props.lastQuoteIndex !== undefined) {
        const last = props.lastQuoteIndex ?? props.totalCount;
        const start = last - props.count + 1;
        const end = last;
        label = `Showing ${start}-${end} from ${props.totalCount}`;
    } else {
        label = `Showing ${props.count} from ${props.totalCount}`;
    }

    return (
        <div className="mb-4 sm:grid sm:grid-cols-4 sm:gap-3">
            <div>
                <div className="overflow-hidden bg-white rounded-lg border">
                    <ul>
                        {props.categories?.map((category, index) => {
                            if (!isCategoriesMinimized || index <= 10) {
                                return <li key={category.id}>
                                    <div
                                        className={
                                            `p-4 ${category.id == selectedCategoryId ?
                                                `text-sky-500 fill-sky-500` :
                                                `text-gray-600 fill-gray-600 hover:text-black`} cursor-pointer`
                                        }
                                        onClick={handleChangeCategory}
                                        data-tid={category.id}
                                    >
                                        <p className="text-lg">{category.title}</p>
                                    </div>
                                </li>;
                            }
                        })}
                    </ul>
                    <div
                        className="flex justify-center items-center p-4 cursor-pointer"
                        onClick={handleShowMore}
                    >
                        {isCategoriesMinimized ? (
                            <p>Show more</p>
                        ) : (
                            <p>Show less</p>
                        )}
                    </div>
                </div>
            </div>
            <div className="overflow-hidden sm:col-span-3">
                <div className="bg-white rounded-lg pb-1">
                    <div className="px-4 py-5 sm:px-6">
                        <h2 className="text-2xl">
                            {label}
                        </h2>
                    </div>
                    <div className="border-t border-gray-200">
                        {children}
                    </div>
                </div>
                <Pagination
                    page={props.page}
                    pageCount={props.pageCount}
                    onChangePage={props.onChangePage}
                />
            </div>
        </div>
    );
}
