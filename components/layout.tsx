import { ReactNode, useState } from "react";

import Link from "next/link";

import Modal from "./modal";
import NavBar from "./navbar";
import navbar from "./navbar.module.css";

interface Props {
    children?: ReactNode
}

const links = (
    <>
        <Link className={navbar.container__link} href="/" key="0">
            Home
        </Link>
        <Link className={navbar.container__link} href="/categories" key="1">
            Categories
        </Link>
        <Link className={navbar.container__link} href="/authors" key="2">
            Authors
        </Link>
    </>
);

export default function Layout({ children, ...props }: Props) {
    const [menuState, setMenuState] = useState(false);
    function handleMenuChangeState() {
        setMenuState(!menuState);
    }

    return (
        <>
            <header className="relative w-full z-50">
                <nav className="flex mx-auto justify-center bg-black">
                    <NavBar onMenuChangeState={handleMenuChangeState}>
                        {links}
                    </NavBar>
                </nav>
            </header>
            <main className="max-w-[1024px] m-auto p-4 sm:p-8">
                {children}
            </main>
            <div
                className={`${menuState ?
                    "absolute" :
                    "hidden"
                    } inset-0 z-40 sm:hidden`
                }
                onTouchStart={() => setMenuState(false)}
            >
                <Modal visible={menuState}>
                    {links}
                </Modal>
            </div>
        </>
    );
}
