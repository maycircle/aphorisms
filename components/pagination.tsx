import { MouseEventHandler } from "react";

interface PaginationOptions {
    page: number;
    pageCount: number;
}

export interface PaginationProps extends PaginationOptions {
    page: number
    pageCount: number
    onChangePage?: (page: number) => void
}

function paginate(options: PaginationOptions, handler: MouseEventHandler) {
    const { page, pageCount } = options;

    const start = 1;
    const pad = 1;
    const n = 3;
    const size = 7;

    let pages = [];

    if (pageCount == 0) {
        if (page > start) {
            pages.push(<PageEllipsis />);
        }

        pages.push(
            <Page
                page={page}
                isActive={true}
            />
        );
        pages.push(<PageEllipsis />);
        return pages;
    }

    if (pageCount <= size) {
        for (let i = start; i <= pageCount; i++) {
            pages.push(
                <Page
                    page={i}
                    isActive={page == i}
                    onChangePage={handler}
                />
            );
        }
    } else if (start + n >= page) {
        let left = Math.max(3, page + 1);
        let right = pageCount - (size - left - 1) + 1;
        for (let i = start; i <= left; i++) {
            pages.push(
                <Page
                    page={i}
                    isActive={page == i}
                    onChangePage={handler}
                />
            );
        }
        pages.push(<PageEllipsis />);
        for (let i = right; i <= pageCount; i++) {
            pages.push(
                <Page
                    page={i}
                    onChangePage={handler}
                />
            );
        }
    } else if (pageCount - n <= page) {
        let right = Math.max(2, pageCount - page + 1) + 1;
        let left = pageCount - right + 1;
        for (let i = start; i <= size - right - 1; i++) {
            pages.push(
                <Page
                    page={i}
                    onChangePage={handler}
                />
            );
        }
        pages.push(<PageEllipsis />);
        for (let i = left; i <= pageCount; i++) {
            pages.push(
                <Page
                    page={i}
                    isActive={page == i}
                    onChangePage={handler}
                />
            );
        }
    } else {
        pages.push(<Page
            page={start}
            onChangePage={handler}
        />);
        pages.push(<PageEllipsis />);
        for (let i = page - pad; i <= page + pad; i++) {
            pages.push(
                <Page
                    page={i}
                    isActive={page == i}
                    onChangePage={handler}
                />
            );
        }
        pages.push(<PageEllipsis />);
        pages.push(<Page
            page={pageCount}
            onChangePage={handler}
        />);
    }

    return pages;
}

export default function Pagination({ ...props }: PaginationProps) {
    const handlePageSet: MouseEventHandler = e => {
        if (props.onChangePage == null) {
            return;
        }

        let pageNumber = e.currentTarget.getAttribute("data-pagenumber");
        if (pageNumber != null) {
            props.onChangePage(parseInt(pageNumber));
        }
    };

    function handlePageNext() {
        if (props.onChangePage == null) {
            return;
        }

        props.onChangePage(props.page + 1);
    }

    function handlePagePrevious() {
        if (props.onChangePage == null) {
            return;
        }

        props.onChangePage(props.page - 1);
    }

    return (
        <div
            className="flex items-center justify-center border-gray-200 bg-white px-4 py-3 sm:px-6"
        >
            <div className="flex sm:flex-1 items-center justify-center">
                <div>
                    <nav
                        className="isolate inline-flex -space-x-px rounded-md shadow-sm"
                        aria-label="Pagination"
                    >
                        <button
                            className="relative hidden items-center rounded-l-md px-2 py-2 text-gray-400 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 sm:inline-flex"
                            onClick={handlePagePrevious}
                            disabled={props.page == 1}
                        >
                            <svg
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                                aria-hidden="true"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z"
                                    clipRule="evenodd"
                                />
                            </svg>
                        </button>
                        {...paginate(props, handlePageSet)}
                        <button
                            className="relative hidden items-center rounded-r-md px-2 py-2 text-gray-400 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 sm:inline-flex"
                            onClick={handlePageNext}
                            disabled={props.page == props.pageCount}
                        >
                            <svg
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                                aria-hidden="true"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z"
                                    clipRule="evenodd"
                                />
                            </svg>
                        </button>
                    </nav>
                </div>
            </div>
        </div>
    );
}

interface PageProps {
    page: Number
    hidden?: boolean
    isActive?: boolean
    onChangePage?: MouseEventHandler
}

function Page({ ...props }: PageProps) {
    if (props.isActive === true) {
        return (
            <span
                aria-current="page"
                className="relative z-10 inline-flex items-center bg-indigo-600 px-3 py-2 text-sm font-semibold text-white focus:z-20 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 sm:px-4"
                data-pagenumber={props.page}
            >
                {props.page.toString()}
            </span>
        );
    }

    return (
        <button
            className={
                `relative ${props.hidden === true && "hidden"} items-center px-3 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 sm:px-4 md:inline-flex`}
            data-pagenumber={props.page}
            onClick={props.onChangePage}
        >
            {props.page.toString()}
        </button>
    );
}

function PageEllipsis() {
    return (
        <span
            className="relative inline-flex items-center px-3 py-2 text-sm font-semibold text-gray-700 ring-1 ring-inset ring-gray-300 focus:outline-offset-0 sm:px-4"
        >
            ...
        </span>
    );
}
