import { ReactNode } from "react";

import styles from "./navbar.module.css";
import Link from "next/link";

interface Props {
    onMenuChangeState?: () => void
    children?: ReactNode
}

export default function NavBar({ children, ...props }: Props) {
    return (
        <div className={styles.navbar}>
            <div className={styles.container}>
                <Link
                    className={styles.container__homeLink}
                    href="/"
                >
                    APHORISMS&trade;
                </Link>
                <div className="h-full hidden sm:flex">
                    {children}
                </div>
                <div className="h-full sm:hidden">
                    <button
                        className={styles.container__link}
                        onClick={props.onMenuChangeState}
                    >
                        <svg
                            viewBox="0 0 24 24"
                            className="h-6 w-6 stroke-white"
                        >
                            <path
                                d="M3.75 12h16.5M3.75 6.75h16.5M3.75 17.25h16.5"
                                fill="none"
                                strokeWidth="1.5"
                                strokeLinecap="round"
                            />
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    );
}
